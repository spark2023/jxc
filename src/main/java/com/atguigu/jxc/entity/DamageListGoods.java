package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 报损商品
 */
public class DamageListGoods {

  private Integer damageListGoodsId;
  private Integer goodsId;
  private String goodsCode;
  private String goodsName;
  private String goodsModel;
  private String goodsUnit;
  private Integer goodsNum;
  private double price;
  private double total;
  private Integer damageListId;
  private Integer goodsTypeId;

  public Integer getDamageListGoodsId() {
    return damageListGoodsId;
  }

  public void setDamageListGoodsId(Integer damageListGoodsId) {
    this.damageListGoodsId = damageListGoodsId;
  }

  public Integer getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(Integer goodsId) {
    this.goodsId = goodsId;
  }

  public String getGoodsCode() {
    return goodsCode;
  }

  public void setGoodsCode(String goodsCode) {
    this.goodsCode = goodsCode;
  }

  public String getGoodsName() {
    return goodsName;
  }

  public void setGoodsName(String goodsName) {
    this.goodsName = goodsName;
  }

  public String getGoodsModel() {
    return goodsModel;
  }

  public void setGoodsModel(String goodsModel) {
    this.goodsModel = goodsModel;
  }

  public String getGoodsUnit() {
    return goodsUnit;
  }

  public void setGoodsUnit(String goodsUnit) {
    this.goodsUnit = goodsUnit;
  }

  public Integer getGoodsNum() {
    return goodsNum;
  }

  public void setGoodsNum(Integer goodsNum) {
    this.goodsNum = goodsNum;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getTotal() {
    return total;
  }

  public void setTotal(double total) {
    this.total = total;
  }

  public Integer getDamageListId() {
    return damageListId;
  }

  public void setDamageListId(Integer damageListId) {
    this.damageListId = damageListId;
  }

  public Integer getGoodsTypeId() {
    return goodsTypeId;
  }

  public void setGoodsTypeId(Integer goodsTypeId) {
    this.goodsTypeId = goodsTypeId;
  }
}
