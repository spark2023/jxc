package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-05 15:52
 */
public interface UnitDao {
    List<Unit> getUnitList();
}
