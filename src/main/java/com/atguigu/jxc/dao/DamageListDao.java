package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;

/**
 * @author terminal
 * @since 2023-07-05 20:46
 */
public interface DamageListDao {
    void saveDamageList(@Param("damageList") DamageList damageList);
}
