package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List<Goods> wareList(@Param("pageSize") Integer pageSize, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer getCount();

    List<Goods> goodsList(@Param("pageSize") Integer pageSize, @Param("rows") Integer rows, @Param("goodsName") String goodsName, @Param("goodsTypeId")Integer goodsTypeId);

    void saveGoods(@Param("goods") Goods goods);

    void updateGoods(@Param("goodsId") Integer goodsId, @Param("goods") Goods goods);

    void deleteById(Integer goodsId);

    Goods getById(Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("pageSize") Integer pageSize, @Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    List<Goods> getInventoryQuantity(@Param("pageSize") Integer pageSize, @Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    void saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);

    void deleteStock(Integer goodsId);
}
