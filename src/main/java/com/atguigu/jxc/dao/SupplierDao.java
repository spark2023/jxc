package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-05 10:47
 */
public interface SupplierDao {
    List<Supplier> supplierList(@Param("pageSize") Integer pageSize, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    Integer count();

    void saveSupplier(@Param("supplier") Supplier supplier, @Param("supplierId") Integer supplierId);

    void deleteIds(@Param("ids") List<Long> ids);
}
