package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-05 20:51
 */
public interface DamageListGoodsDao {
    void saveDamageListGoods(@Param("damageListGoods") List<DamageListGoods> damageListGoods);
}
