package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    Map<String, Object> wareList(Integer pageSize, Integer rows, String codeOrName, Integer goodsTypeId);

    Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void saveGoods(Goods goods);

    void updateGoods(Integer goodsId, Goods goods);

    void removeById(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    void deleteStock(Integer goodsId);
}
