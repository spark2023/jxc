package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

/**
 * @author terminal
 * @since 2023-07-05 20:46
 */
public interface DamageListService {
    void damageListSave(DamageList damageList, String damageListGoodsStr);
}
