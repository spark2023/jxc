package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 10:42
 */
public interface SupplierService {
    Map<String, Object> supplierList(Integer page, Integer rows, String supplierName);

    void saveSupplier(Supplier supplier, Integer supplierId);

    void deleteIds(List<Long> ids);
}
