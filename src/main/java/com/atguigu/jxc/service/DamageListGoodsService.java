package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-05 20:50
 */
public interface DamageListGoodsService {
    void save(Integer damageListId, List<DamageListGoods> damageListGoods);
}
