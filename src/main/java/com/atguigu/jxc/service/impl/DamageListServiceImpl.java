package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.DamageListService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-05 20:46
 */
@Service
public class DamageListServiceImpl implements DamageListService {
    @Autowired
    private DamageListDao damageListDao;
    @Autowired
    private DamageListGoodsService damageListGoodsService;
    @Override
    public void damageListSave(DamageList damageList, String damageListGoodsStr) {
        //先把damageListGoodsStr转为集合
        List<DamageListGoods> damageListGoods = JSON.parseArray(damageListGoodsStr, DamageListGoods.class);
        //保存报损单
        damageListDao.saveDamageList(damageList);
        //保存报损单明细
        //获取报损单主键
        Integer damageListId = damageList.getDamageListId();
        damageListGoodsService.save(damageListId,damageListGoods);
    }
}
