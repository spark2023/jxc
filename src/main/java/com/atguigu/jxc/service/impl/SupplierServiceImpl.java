package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 10:42
 */
@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao;

    @Override
    public Map<String, Object> supplierList(Integer page, Integer rows, String supplierName) {
        Map<String, Object> mapResult = new HashMap<>();
        page = page == 0 ? 1 : page;
        //每页开始条数
        Integer pageSize = (page - 1) * rows;
        List<Supplier> supplierList = supplierDao.supplierList(pageSize, rows, supplierName);
        Integer count = supplierDao.count();
        mapResult.put("rows", supplierList);
        mapResult.put("total", count);
        return mapResult;
    }

    @Override
    public void saveSupplier(Supplier supplier, Integer supplierId) {
        supplierDao.saveSupplier(supplier,supplierId);
    }

    @Override
    public void deleteIds(List<Long> ids) {
        supplierDao.deleteIds(ids);
    }
}
