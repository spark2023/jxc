package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 15:51
 */
@Service
public class UnitServiceImpl implements UnitService {
    @Autowired
    private UnitDao unitDao;
    @Override
    public Map<String, Object> getUnitList() {
      List<Unit> unitList = unitDao.getUnitList();
        Map<String,Object> mapResult = new HashMap<>();
        mapResult.put("rows",unitList);
        return mapResult;
    }
}
