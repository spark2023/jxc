package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author terminal
 * @since 2023-07-05 20:50
 */
@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {
    @Autowired
    private DamageListGoodsDao damageListGoodsDao;
    @Override
    public void save(Integer damageListId, List<DamageListGoods> damageListGoods) {
        damageListGoods.stream().forEach(damageListGood -> {
            damageListGood.setDamageListId(damageListId);
        });
        damageListGoodsDao.saveDamageListGoods(damageListGoods);
    }
}
