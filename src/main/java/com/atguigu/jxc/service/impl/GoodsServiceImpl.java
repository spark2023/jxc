package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /*
    根据商品类别、商品编码、或商品名称搜索
     */
    @Override
    public Map<String, Object> wareList(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        page = page == 0 ? 1 : page;
        //计算每页起始条数
        Integer pageSize = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.wareList(pageSize, rows, codeOrName, goodsTypeId);
        //返回库存总条数
        Integer goodsCount = goodsDao.getCount();
        //定义返回结果
        Map<String, Object> wareResult = new HashMap<>();
        wareResult.put("total", goodsCount);
        wareResult.put("rows", goodsList);
        return wareResult;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        page = page == 0 ? 1 : page;
        //当前页起始条数
        Integer pageSize = (page - 1)*rows;
      List<Goods> goodsList = goodsDao.goodsList(pageSize,rows,goodsName,goodsTypeId);
        //返回库存总条数
        Integer goodsCount = goodsDao.getCount();
        Map<String,Object> goodsMap = new HashMap<>();
        goodsMap.put("total",goodsCount);
        goodsMap.put("rows",goodsList);
        return goodsMap;
    }

    @Override
    public void saveGoods(Goods goods) {
        goodsDao.saveGoods(goods);
    }

    @Override
    public void updateGoods(Integer goodsId, Goods goods) {
        goodsDao.updateGoods(goodsId,goods);
    }

    @Override
    public void removeById(Integer goodsId) {
        //根据goodsId先查询,判断
       Goods goods = goodsDao.getById(goodsId);
       if(goods.getState() ==2 || goods.getState()==1){
           throw new RuntimeException("不允许删除商品");
       }
        goodsDao.deleteById(goodsId);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page==0?1:page;
        //每页开始条数
        Integer pageSize = (page -1 ) * rows;
       List<Goods> noInventoryQuantityGoodsList = goodsDao.getNoInventoryQuantity(pageSize,rows,nameOrCode);
        //返回库存总条数
        Integer goodsCount = goodsDao.getCount();
        Map<String,Object> goodsMap = new HashMap<>();
        goodsMap.put("total",goodsCount);
        goodsMap.put("rows",noInventoryQuantityGoodsList);
        return goodsMap;
    }

    @Override
    public Map<String, Object> getInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        page = page==0?1:page;
        //每页开始条数
        Integer pageSize = (page -1 ) * rows;
        List<Goods> inventoryQuantityGoodsList = goodsDao.getInventoryQuantity(pageSize,rows,nameOrCode);
        //返回库存总条数
        Integer goodsCount = goodsDao.getCount();
        Map<String,Object> goodsMap = new HashMap<>();
        goodsMap.put("total",goodsCount);
        goodsMap.put("rows",inventoryQuantityGoodsList);
        return goodsMap;
    }

    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveStock(goodsId,inventoryQuantity,purchasingPrice);
    }

    @Override
    public void deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getById(goodsId);
        if(goods.getState() ==2 || goods.getState()==1){
            throw new RuntimeException("不允许删除商品");
        }
        goodsDao.deleteStock(goodsId);
    }
}
