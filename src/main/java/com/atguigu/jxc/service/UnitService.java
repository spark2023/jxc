package com.atguigu.jxc.service;

import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 15:51
 */
public interface UnitService {
    Map<String, Object> getUnitList();
}
