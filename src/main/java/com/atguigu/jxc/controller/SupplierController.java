package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 10:38
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    /**
     * 查询供应商列表
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @PostMapping("list")
    public Map<String,Object> supplierList(@RequestParam("page") Integer page, @RequestParam("rows") Integer rows,
                                           @RequestParam(value = "supplierName",required = false) String supplierName){
      Map<String,Object> supplierList = supplierService.supplierList(page,rows,supplierName);
      return supplierList;
    }

    @PostMapping("/save")
    public ServiceVO saveSupplier(Supplier supplier, @RequestParam(value = "supplierId",required = false) Integer supplierId){
        supplierService.saveSupplier(supplier,supplierId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }
    @PostMapping("/delete")
    public ServiceVO deleteIds(@RequestParam List<Long> ids){
        supplierService.deleteIds(ids);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }
}
