package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListService;
import com.atguigu.jxc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author terminal
 * @since 2023-07-05 20:37
 */
@RestController
public class DamageListController {
    @Autowired
    private DamageListService damageListService;
    /**
     * 保存商品报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/damageListGoods/save")
    public ServiceVO damageListSave(DamageList damageList, String damageListGoodsStr, HttpSession session){
        User user = (User)session.getAttribute("currentUser");
        //获取登录id
        Integer userId = user.getUserId();
        damageList.setUserId(userId);
        damageListService.damageListSave(damageList,damageListGoodsStr);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }
}
