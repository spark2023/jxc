package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/goods/listInventory")
    public Map<String,Object> wareList(@RequestParam("page") Integer page, @RequestParam("rows") Integer rows,
                                       @RequestParam(value = "codeOrName",required = false) String codeOrName,
                                       @RequestParam(value="goodsTypeId",required = false) Integer goodsTypeId){
      Map<String,Object> wareResult = goodsService.wareList(page,rows,codeOrName,goodsTypeId);
      return wareResult;
    }

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/goods/list")
    public Map<String,Object> goodsList(@RequestParam("page") Integer page, @RequestParam("rows") Integer rows,
                                       @RequestParam(value = "goodsName",required = false) String goodsName,
                                       @RequestParam(value="goodsTypeId",required = false) Integer goodsTypeId){
       Map<String,Object> goodsMap = goodsService.getGoodsList(page,rows,goodsName,goodsTypeId);
       return goodsMap;
    }
    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/goods/save")
    public ServiceVO saveOrUpdate(@RequestBody Goods goods,@RequestParam("goodsId") Integer goodsId){
        if(goodsId == null){
            //保存
            goodsService.saveGoods(goods);
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
        }else{
            //修改
            goodsService.updateGoods(goodsId,goods);
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
        }
    }

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/goods/delete")
    public ServiceVO removeGoods(@RequestParam Integer goodsId){
        goodsService.removeById(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/goods/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(@RequestParam("page") Integer page,@RequestParam("rows") Integer rows,
                                                     @RequestParam(value = "nameOrCode",required = false) String nameOrCode){
       Map<String,Object> noInventoryQuantityMap = goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
       return noInventoryQuantityMap;
    }

    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/goods/getInventoryQuantity")
    public Map<String,Object> getInventoryQuantity(@RequestParam("page") Integer page,@RequestParam("rows") Integer rows,
                                                     @RequestParam(value = "nameOrCode",required = false) String nameOrCode){
        Map<String,Object> inventoryQuantityMap = goodsService.getInventoryQuantity(page,rows,nameOrCode);
        return inventoryQuantityMap;
    }

    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @PostMapping("/goods/saveStock")
    public ServiceVO saveStock(@RequestParam("goodsId") Integer goodsId,@RequestParam(value = "inventoryQuantity",required = false) Integer inventoryQuantity,@RequestParam(value = "purchasingPrice",required = false) double purchasingPrice){
        goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
//    @PostMapping("/goods/delete")
//    public ServiceVO deleteStock(@RequestParam Integer goodsId){
//        goodsService.deleteStock(goodsId);
//        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
//    }
    /**
     * 查询库存报警商品信息
     * @return
     */

}
