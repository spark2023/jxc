package com.atguigu.jxc.controller;

import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author terminal
 * @since 2023-07-05 15:50
 */
@RestController
public class UnitController {
    @Autowired
    private UnitService unitService;
    @PostMapping("/unit/list")
    public Map<String,Object> getUnitList(){
        Map<String,Object> unitMap = unitService.getUnitList();
        return unitMap;
    }
}
